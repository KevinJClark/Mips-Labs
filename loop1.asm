## t0 - point to array elements in turn 
## t1 - contains count of elements 
## t2 - contains sum 
## t3 - each word from array in turn
## t4 - loop counter

.data
array:			.word 5 4 4 6
arraysize:		.word 4
newline:		.asciiz "\n"
sumprompt:	.asciiz "The sum is: "

.text 
.globl __start

main:
	la $t0 array
	lw $t1 arraysize
	li $t2 0
	li $t3 0
	li $t4 0			#loop counter
	
	looperoo:				#Effectively a do-for loop: for(int t4; t4 < arraysize; t4++)
		move $a0 $t3 	#Prints the current value in the array
		li $v0 1
		syscall
		
		la $a0 newline	#Prints a newline character
		li $v0 4
		syscall
	
		lw $t3 ($t0)
		add $t0 $t0 4
		add $t2 $t2 $t3
		add $t4 $t4 1
		blt $t4 $t1 looperoo

	#Display stuff
	move $a0 $t3 	#Prints the current value in the array
	li $v0 1
	syscall
		
	la $a0 newline	#Prints a newline character
	li $v0 4
	syscall
	
	la $a0 sumprompt	#Prints a prompt
	li $v0 4
	syscall

	move $a0 $t2
	li $v0 1
	syscall
	
	la $a0 newline	#Prints a newline character
	li $v0 4
	syscall
	
	li $v0 10
	syscall #exits
