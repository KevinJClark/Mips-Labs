.data
prompt:  .asciiz "Enter 2 int here: "
prompt2: .asciiz "You entered: "
prompt3: .asciiz "The min and max values were: "
space:	 .asciiz " "
newline: .asciiz "\n"


.text
.globl __start

#######################
main:
	jal readtwo
	move $t2 $t0
	move $t3 $t1
	jal readtwo
	jal printback
	jal findmax
	jal findmin
	jal printminmax

	li $v0 10
	syscall

#######################
readtwo:
	la $a0 prompt	#Print out prompt
	li $v0 4
	syscall

	li $v0 5	#Read in integer
	syscall
	move $t0 $v0

	li $v0 5	#Read in a second integer
	syscall
	move $t1 $v0

	jr $ra		#Return to main function

#######################
printminmax:
	la $a0 prompt3	#Prints a prompt
	li $v0 4
	syscall
	
	move $a0 $t5	#Prints the value in $t5
	li $v0 1
	syscall
	
	la $a0, space	#Prints a space character
	li $v0 4
	syscall
	
	move $a0 $t4	#Prints the value in $t4
	li $v0 1
	syscall
	
	jr $ra		#Return
#######################
printback:
	la $a0 prompt2	#Prints a prompt
	li $v0 4
	syscall
	move $a0 $t2	#Prints the value in $t2
	li $v0 1
	syscall
	la $a0, space	#Prints a space character
	li $v0 4
	syscall

	move $a0 $t3	#Prints the value in $t3
	li $v0 1
	syscall
	la $a0 space	#Prints a space character
	li $v0 4
	syscall

	move $a0 $t0	#Prints the value in $t0
	li $v0 1
	syscall
	la $a0 space	#Prints a space character
	li $v0 4
	syscall
		
	move $a0 $t1	#Prints the value in $t1
	li $v0 1	
	syscall
	la $a0 newline	#Prints a newline character
	li $v0 4
	syscall

	jr $ra		#Return to main function

####################### #Saves into register $t4
findmax:
	fmx1:
		move $t4 $t0
		blt $t1 $t4 fmx2	#if($t1 < $t4)  goto fmx2				fmx1	 -------------------------- Binary tree structure for comparisons
		b fmx3			#must be greater than or equal to	    fmx2		      fm3		    Less than on the left, GTE goes right
	fmx2:								 # fmx4		  fmx5		fmx4           fmx5
		#No move					  #   fmx6      fmx7  fmx6    fmx7   fmx6   fmx7   fmx6    fmx7
		blt $t2 $t4 fmx4
		b fmx5
	fmx3:
		move $t4 $t1
		blt $t2 $t4 fmx6
		b fmx7
	fmx4:
		#No move
		blt $t3 $t4 fmx6
		b fmx7
	fmx5:
		move $t0 $t2
		blt $t3 $t4 fmx6
		b fmx7
	fmx6:
		#No move
		jr $ra
	fmx7:
		move $t4 $t3
		jr $ra

######################## #Uses register $t5 instead to save into
findmin:
	fmn1:
		move $t5 $t0
		bgt $t1 $t5 fmn2
		b fmn3
	fmn2:
		#No move
		bgt $t2 $t5 fmn4
		b fmn5
	fmn3:
		move $t5 $t1
		bgt $t2 $t0 fmn6
		b fmn7
	fmn4:
		#No move
		bgt $t3 $t5 fmn6
		b fmn7
	fmn5:
		move $t5 $t2
		bgt $t3 $t5 fmn6
		b fmn7
	fmn6:
		#No move
		jr $ra
	fmn7:
		move $t5 $t3
		jr $ra
